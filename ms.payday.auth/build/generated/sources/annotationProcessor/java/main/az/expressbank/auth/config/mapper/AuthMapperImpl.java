package az.expressbank.auth.config.mapper;

import az.expressbank.auth.model.dto.RegisterDto;
import az.expressbank.auth.model.dto.RegisterRequestDto;
import az.expressbank.auth.model.dto.RegisterRequestDto.RegisterRequestDtoBuilder;
import az.expressbank.auth.model.dto.UserDto;
import az.expressbank.auth.model.dto.UserDto.UserDtoBuilder;
import az.expressbank.auth.model.entity.UserEnt;
import az.expressbank.auth.model.entity.UserEnt.UserEntBuilder;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-01-10T21:55:43+0400",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 11.0.15.1 (BellSoft)"
)
@Component
public class AuthMapperImpl implements AuthMapper {

    @Override
    public UserEnt mapRegisterDtoToEnt(RegisterDto registerDto) {
        if ( registerDto == null ) {
            return null;
        }

        UserEntBuilder userEnt = UserEnt.builder();

        userEnt.name( setName( registerDto.getName() ) );
        userEnt.password( setPassword( registerDto.getPassword() ) );
        userEnt.enabled( setEnabled( registerDto.getName() ) );
        userEnt.verificationCode( setVerificationCode( registerDto.getName() ) );
        userEnt.email( registerDto.getEmail() );

        return userEnt.build();
    }

    @Override
    public RegisterRequestDto mapEntToRegisterDto(UserEnt userEnt) {
        if ( userEnt == null ) {
            return null;
        }

        RegisterRequestDtoBuilder registerRequestDto = RegisterRequestDto.builder();

        registerRequestDto.name( userEnt.getName() );
        registerRequestDto.email( userEnt.getEmail() );

        return registerRequestDto.build();
    }

    @Override
    public UserDto mapEntToDto(UserEnt userEnt) {
        if ( userEnt == null ) {
            return null;
        }

        UserDtoBuilder userDto = UserDto.builder();

        userDto.name( userEnt.getName() );
        userDto.email( userEnt.getEmail() );
        userDto.password( userEnt.getPassword() );
        userDto.id( userEnt.getId() );

        return userDto.build();
    }
}
