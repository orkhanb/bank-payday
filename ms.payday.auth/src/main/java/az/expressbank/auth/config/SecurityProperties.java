package az.expressbank.auth.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;

@Getter
@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "security")
public class SecurityProperties {

    private final JwtProperties jwtProperties = new JwtProperties();
    private final CorsConfiguration cors = new CorsConfiguration();

    @Getter
    @Setter
    public class JwtProperties {
        private String secret;
        private long tokenValidityInSeconds;
        private long tokenValidityInSecondsForRememberMe;
    }

}
