package az.expressbank.auth.config.mapper;

import az.expressbank.auth.model.dto.RegisterDto;
import az.expressbank.auth.model.dto.RegisterRequestDto;
import az.expressbank.auth.model.dto.UserDto;
import az.expressbank.auth.model.entity.UserEnt;
import net.bytebuddy.utility.RandomString;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.ReportingPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface AuthMapper {
    @Mapping(target = "name", source = "name", qualifiedByName = "setName")
    @Mapping(target = "password", source = "password", qualifiedByName = "setPassword")

    @Mapping(target = "enabled", source = "name", qualifiedByName = "setEnabled")
    @Mapping(target = "verificationCode", source = "name", qualifiedByName = "setVerificationCode")
    UserEnt mapRegisterDtoToEnt(RegisterDto registerDto);

    RegisterRequestDto mapEntToRegisterDto(UserEnt userEnt);

    UserDto mapEntToDto(UserEnt userEnt);

    @Named("setName")
    default String setName(String name) {
        return name.toLowerCase();
    }

    @Named("setPassword")
    default String setPassword(String password) {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        return bCryptPasswordEncoder.encode(password);
    }

    @Named("setVerificationCode")
    default String setVerificationCode(String verification) {
        return RandomString.make(64);
    }

    @Named("setEnabled")
    default Boolean setEnabled(String enabled) {
        return false;
    }
}