package az.expressbank.auth.config.jwt;

import az.expressbank.auth.config.SecurityProperties;
import az.expressbank.auth.model.dto.UserDto;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.security.Key;
import java.time.Instant;
import java.util.Date;
import java.util.Map;

@Slf4j
@Service
public class JwtService {

    @Getter
    private Key key;

    @Autowired
    private SecurityProperties properties;

    @PostConstruct
    public void init() {
        byte[] keyBytes = Decoders.BASE64.decode(properties.getJwtProperties().getSecret());
        key = Keys.hmacShaKeyFor(keyBytes);
    }

    public Claims parseToken(String token) {
        return Jwts.parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(token)
                .getBody();
    }

    public String issueToken(UserDto userDto) {
        log.trace("Issue JWT token to {} for {} seconds", userDto, properties.getJwtProperties().getTokenValidityInSeconds());
        final JwtBuilder jwtBuilder = Jwts.builder()
                .setSubject(userDto.getName())
                .setIssuedAt(new Date())
                .setHeader(Map.of("type", "JWT"))
                .addClaims(Map.of("id", userDto.getId()))
                .addClaims(Map.of("name", userDto.getName()))
                .setExpiration(Date.from(Instant.now().plusSeconds(properties.getJwtProperties().getTokenValidityInSeconds())))
                .signWith(key, SignatureAlgorithm.HS512);
        return jwtBuilder.compact();
    }

}
