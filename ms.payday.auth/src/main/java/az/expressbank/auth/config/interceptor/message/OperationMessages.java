package az.expressbank.auth.config.interceptor.message;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum OperationMessages {
    USER_NOT_FOUND_MSG("auth.exception.user.exist"),
    USER_ALREADY_SET("auth.exception.user.already.set"),
    EMAIL_ALREADY_SET("auth.exception.email.exist"),
    INCORRECT_PASSWORD_FORMAT("auth.exception.password.format");

    @Getter
    private final String operationMessage;
}
