package az.expressbank.auth.config;

import az.expressbank.auth.config.jwt.JwtAuthFilterConfigurerAdapter;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

@Configuration
@RequiredArgsConstructor
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private static final String ACTUATOR = "/actuator/**";
    private static final String SWAGGER2 = "/v2/api-docs";
    private static final String SWAGGER3 = "/v3/api-docs";
    private static final String SWAGGER_UI = "/swagger-ui/**";
    private static final String SWAGGER_HTML = "/swagger-ui.html";


    private final JwtAuthFilterConfigurerAdapter jwtFilter;

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/v2/api-docs",
                "/configuration/ui",
                "/swagger-resources/**",
                "/configuration/security",
                "/swagger-ui.html",
                "/webjars/**");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .cors().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests().antMatchers(HttpMethod.POST, "/ms/auth/register").permitAll()
                .and()
                .authorizeRequests().antMatchers(HttpMethod.POST, "/ms/auth/login").permitAll()
                .and()
                .authorizeRequests().antMatchers(HttpMethod.GET, "/ms/auth/verify").permitAll()
                .and()
                .authorizeRequests().antMatchers(ACTUATOR, SWAGGER2, SWAGGER3, SWAGGER_HTML, SWAGGER_UI);
        http.apply(jwtFilter);

        super.configure(http);
    }


}
