package az.expressbank.auth.config.jwt;

import io.jsonwebtoken.*;
import io.jsonwebtoken.security.SignatureException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public final class TokenAuthService implements AuthService {

    public static final String ROLES_CLAIM = "role";
    public static final String AUTH_HEADER = "Authorization";
    public static final String BEARER_AUTH_HEADER = "Bearer";
    private final JwtService jwtService;

    @Override
    public Optional<Authentication> getAuthentication(HttpServletRequest req) {
        return Optional.ofNullable(getHeader(req))
                .filter(this::isBearerAuth)
                .flatMap(this::getAuthenticationBearer);
    }

    private String getHeader(HttpServletRequest req) {
        var header = req.getHeader(AUTH_HEADER);
        if (header == null && !req.getRequestURI().contains("internal")) {
            log.error("'Authorization' header is missing!");
        }
        return header;
    }

    private boolean isBearerAuth(String header) {
        return header.toLowerCase().startsWith(BEARER_AUTH_HEADER.toLowerCase());
    }

    private Optional<Authentication> getAuthenticationBearer(String header) {
        var token = header.substring(BEARER_AUTH_HEADER.length()).trim();
        if (!validateJwtToken(token)) {
            return Optional.empty();
        }
        var claims = jwtService.parseToken(token);
        //log.info("The claims parsed {}", claims);
        return Optional.of(getAuthenticationBearer(claims));
    }

    private Authentication getAuthenticationBearer(Claims claims) {
        List<?> roles = claims.get(ROLES_CLAIM, List.class);
        List<GrantedAuthority> authorityList = roles.stream()
                .map(role -> new SimpleGrantedAuthority(role.toString()))
                .collect(Collectors.toList());
        return new UsernamePasswordAuthenticationToken(claims, "", authorityList);
    }

    public boolean validateJwtToken(String authToken) {
        try {
            Jwts.parserBuilder().setSigningKey(jwtService.getKey()).build().parseClaimsJws(authToken);
            return true;
        } catch (ExpiredJwtException e) {
            log.error("Expired: {}", e.getMessage());
        } catch (MalformedJwtException e) {
            log.error("Invalid JWT token: {}", e.getMessage());
        } catch (UnsupportedJwtException e) {
            log.error("JWT token is unsupported: {}", e.getMessage());
        } catch (IllegalArgumentException e) {
            log.error("JWT claims string is empty: {}", e.getMessage());
        } catch (SignatureException e) {
            log.error("JWT signature does not match locally computed signature: {}", e.getMessage());
        }
        return false;
    }

}
