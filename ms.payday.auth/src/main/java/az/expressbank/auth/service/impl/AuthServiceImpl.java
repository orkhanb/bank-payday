package az.expressbank.auth.service.impl;

import az.expressbank.auth.client.EmailClient;
import az.expressbank.auth.config.jwt.JwtService;
import az.expressbank.auth.config.mapper.AuthMapper;
import az.expressbank.auth.exception.AuthException;
import az.expressbank.auth.model.dto.*;
import az.expressbank.auth.model.entity.UserEnt;
import az.expressbank.auth.repository.UserRepo;
import az.expressbank.auth.service.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Locale;
import java.util.Optional;

import static az.expressbank.auth.config.interceptor.message.OperationMessages.*;

@Component
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService, UserDetailsService {

    private final MessageSource messageSource;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final AuthMapper authMapper;
    private final UserRepo userRepo;
    private final JwtService jwtService;
    private final EmailClient emailClient;

    @Override
    public RegisterRequestDto register(RegisterDto registerDto) {
        Locale locale = LocaleContextHolder.getLocale();
        System.out.println("service: " + messageSource.getMessage(USER_ALREADY_SET.getOperationMessage(), new Object[]{}, locale));
        Optional<UserEnt> userEntTemp = userRepo.findByName(registerDto.getName());
        Optional<UserEnt> userEntEmailCntrl = userRepo.findByEmail(registerDto.getEmail());
        if (userEntTemp.isPresent()) {
            throw new AuthException.UserAlreadyExistAuthenticationException(
                    String.format(
                            messageSource.getMessage(USER_ALREADY_SET.getOperationMessage(), new Object[]{}, locale),
                            registerDto.getName())
            );
        }

        if (userEntEmailCntrl.isPresent()) {
            throw new AuthException.UserAlreadyExistAuthenticationException(
                    String.format(
                            messageSource.getMessage(EMAIL_ALREADY_SET.getOperationMessage(), new Object[]{}, locale),
                            registerDto.getEmail()));
        }

        UserEnt userEnt = authMapper.mapRegisterDtoToEnt(registerDto);
        UserEnt userSaveEnt = userRepo.save(userEnt);
        RegisterRequestDto userSaveDto = authMapper.mapEntToRegisterDto(userSaveEnt);
        try {
            sendVerificationEmail(userSaveEnt, "http://localhost:9001/ms/auth");
        } catch (Exception e) {
            throw new RuntimeException("SMS Error: " + e.getMessage());
        } finally {
            return userSaveDto;
        }
    }

    private void sendVerificationEmail(UserEnt user, String siteURL)
    // throws MessagingException, UnsupportedEncodingException
    {
        String toAddress = user.getEmail();
        String fromAddress = "frombakuo@gmail.com";
        String senderName = "UFO";
        String subject = "Please verify your registration";
        String content = "Dear [[name]],<br>"
                + "Please click the link below to verify your registration:<br>"
                + "<h3><a href=\"[[URL]]\" target=\"_self\">VERIFY</a></h3>"
                + "Thank you,<br>"
                + "Your company name.";


        content = content.replace("[[name]]", user.getName());
        String verifyURL = siteURL + "/verify?code=" + user.getVerificationCode();
        content = content.replace("[[URL]]", verifyURL);
        ;
        emailClient.sendEmail(SendMessageRequestDto.builder()
                .toAddress(toAddress)
                .fromAddress(fromAddress)
                .senderName(senderName)
                .subject(subject)
                .senderName("UFO")
                .subject(content).build());
    }

    @Override
    public LoginResponseDto login(LoginRequestDto request) {
        var userDetails = (UserEnt) loadUserByUsername(request.getName());
        boolean matches = bCryptPasswordEncoder.matches(request.getPassword(), userDetails.getPassword());
        if (!matches) {
            throw new AuthException.UserNameOrPasswordInvalid("Email or password is invalid.");
        }
        if (!userDetails.getEnabled()) {
            throw new AuthException.UserIsNotEnable("User is not active.");
        }

        UserDto userDto = authMapper.mapEntToDto(userDetails);
        LoginResponseDto loginResponseDto = LoginResponseDto.builder()
                .jwt(jwtService.issueToken(userDto))
                .build();
        return loginResponseDto;
    }

    @Override
    public RegisterRequestDto verify(String verificationCode) {
        Optional<UserEnt> userEnt = userRepo.findByVerificationCode(verificationCode);
        if (!userEnt.isPresent() || userEnt.get().getEnabled()) {
            return null;
        }
        userEnt.get().setEnabled(true);
        userEnt.get().setVerificationCode(null);
        UserEnt userSaveEnt = userRepo.save(userEnt.get());
        RegisterRequestDto userSaveDto = authMapper.mapEntToRegisterDto(userSaveEnt);
        return userSaveDto;
    }

    @Override
    public UserDetails loadUserByUsername(String name) {
        Locale locale = LocaleContextHolder.getLocale();
        return userRepo.findByName(name).orElseThrow(() -> new UsernameNotFoundException(
                String.format(
                        messageSource.getMessage(
                                USER_NOT_FOUND_MSG.getOperationMessage(), new Object[]{}, locale)
                        , name)
        ));

    }

}
