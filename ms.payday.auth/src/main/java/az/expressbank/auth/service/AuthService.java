package az.expressbank.auth.service;

import az.expressbank.auth.model.dto.LoginRequestDto;
import az.expressbank.auth.model.dto.LoginResponseDto;
import az.expressbank.auth.model.dto.RegisterDto;
import az.expressbank.auth.model.dto.RegisterRequestDto;

public interface AuthService {
    RegisterRequestDto register(RegisterDto registerDto);

    LoginResponseDto login(LoginRequestDto loginRequestDto);

    RegisterRequestDto verify(String verificationCode);
}
