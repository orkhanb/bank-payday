package az.expressbank.auth.client;

import az.expressbank.auth.model.dto.SendMessageRequestDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(url = "${url.ms.email}", name = "emailClient")
public interface EmailClient {

    @PostMapping("verify")
    Void sendEmail(@RequestBody SendMessageRequestDto request);

}