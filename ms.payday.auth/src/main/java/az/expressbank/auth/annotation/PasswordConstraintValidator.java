package az.expressbank.auth.annotation;

import az.expressbank.auth.exception.AuthException;
import lombok.SneakyThrows;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

import static az.expressbank.auth.model.consts.ErrorMessage.INCORRECT_PASSWORD_FORMAT;

public class PasswordConstraintValidator implements ConstraintValidator<Password, String> {

    @SneakyThrows
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        var result = Pattern
                .compile("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[#.?!@$%^&\\-*]).{6,}$")
                .matcher(value)
                .matches();

        if (!result) throw new AuthException.PasswordValidationException(INCORRECT_PASSWORD_FORMAT);

        return true;
    }
}
