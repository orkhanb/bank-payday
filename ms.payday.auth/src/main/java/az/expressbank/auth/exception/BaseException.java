package az.expressbank.auth.exception;

import lombok.EqualsAndHashCode;
import org.springframework.security.core.AuthenticationException;


@EqualsAndHashCode(callSuper = false)
public abstract class BaseException extends AuthenticationException {
    private String message;

    public BaseException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public BaseException(String msg) {
        super(msg);
    }
}

