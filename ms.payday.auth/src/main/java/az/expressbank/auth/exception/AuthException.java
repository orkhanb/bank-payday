package az.expressbank.auth.exception;

import org.springframework.security.core.AuthenticationException;

public class AuthException {

    public static class UserAlreadyExistAuthenticationException extends AuthenticationException {
        private static final long serialVersionUID = 4235225697094262603L;

        public UserAlreadyExistAuthenticationException(String msg) {
            super(msg);
        }
    }
    public static class PasswordValidationException extends AuthenticationException {
        private static final long serialVersionUID = 4235225697941626032L;

        public PasswordValidationException(String msg) {
            super(msg);
        }
    }
    public static class UserNameOrPasswordInvalid extends BaseException {
        private static final long serialVersionUID = 3555714415375055302L;

        public UserNameOrPasswordInvalid(String message) {
            super(message);
        }
    }

    //No data exist where we expect at least one row
    public static class UserIsNotEnable extends BaseException {
        private static final long serialVersionUID = 8777415230393628334L;

        public UserIsNotEnable(String msg) {
            super(msg);
        }
    }

    //Multiple rows exist where we expect only single row
    public static class MoreData extends BaseException {
        private static final long serialVersionUID = -3987707665150073980L;

        public MoreData(String msg) {
            super(msg);
        }
    }

    //Invalid parameters error
    public static class InvalidParam extends BaseException {
        private static final long serialVersionUID = 4235225697094262603L;

        public InvalidParam(String msg) {
            super(msg);
        }
    }

}
