package az.expressbank.auth.model.dto;

import az.expressbank.auth.annotation.Password;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.*;
import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RegisterDto {

    @NotNull
    @NotBlank
    @Size(min = 5, max = 20)
    String name;

    @NotEmpty
    @Password
    String password;

    @Email
    String email;



}
