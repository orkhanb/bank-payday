package az.expressbank.auth.model.consts;

public final class Header {
    public static final String AUTHORIZATION = "Authorization";
    public static final String LOGIN_NAME = "Login-Name";
}
