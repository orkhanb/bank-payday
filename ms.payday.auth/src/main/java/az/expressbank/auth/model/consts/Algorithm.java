package az.expressbank.auth.model.consts;

public final class Algorithm {
    public static final String PBKDF2WithHmacSHA1 = "PBKDF2WithHmacSHA1";
    public static final String X509 = "X.509";
}
