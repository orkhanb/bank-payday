package az.expressbank.auth.model.dto;


import az.expressbank.auth.annotation.Password;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotEmpty;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class LoginRequestDto {

    @NotEmpty
    String name;
    @NotEmpty
    @Password
    String password;
}
