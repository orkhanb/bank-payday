package az.expressbank.auth.model.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserDto {
    @NotNull
    @NotBlank
    @Size(min = 7, max = 20)
    String name;

    @Email
    String email;

    @Size(min = 7, max = 20)
    String password;

    Long id;
}
