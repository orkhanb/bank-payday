package az.expressbank.auth.model.dto;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SendMessageRequestDto {
    String toAddress;
    String fromAddress;
    String senderName;
    String subject;
    String content;
}