package az.expressbank.auth.model.consts;

public final class ErrorMessage {
    public static final String USER_NOT_FOUND = "User not found";
    public static final String BAD_CREDENTIALS = "LoginName or password is incorrect";
    public static final String INVALID_TOKEN = "Token is invalid";
    public static final String ACCESS_TOKEN_EXPIRED = "Access token is expired";
    public static final String LOGINNAME_ALREADY_EXISTS = "User with this loginName already exists";
    public static final String INCORRECT_PASSWORD_FORMAT = "Incorrect password format";

}
