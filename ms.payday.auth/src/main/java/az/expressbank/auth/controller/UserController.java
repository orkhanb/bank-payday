package az.expressbank.auth.controller;

import az.expressbank.auth.annotation.Api;
import az.expressbank.auth.model.dto.LoginRequestDto;
import az.expressbank.auth.model.dto.RegisterDto;
import az.expressbank.auth.service.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(path = "/ms/auth")
@RequiredArgsConstructor
public class UserController {

    private final AuthService authService;

    @PostMapping("/register")
    public ResponseEntity<?> register(@RequestBody @Valid RegisterDto registerDto) {

        return ResponseEntity.ok(authService.register(registerDto));
    }

    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody @Valid LoginRequestDto loginRequestDto) {
        return ResponseEntity.ok(authService.login(loginRequestDto));
    }

    @GetMapping("/verify")
    public ResponseEntity<?> verify(@Valid @RequestParam("code") String code) {
        return ResponseEntity.ok(authService.verify(code));
    }
}
