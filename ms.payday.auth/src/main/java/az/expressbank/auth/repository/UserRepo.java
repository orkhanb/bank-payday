package az.expressbank.auth.repository;

import az.expressbank.auth.model.entity.UserEnt;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public interface UserRepo extends JpaRepository<UserEnt, Long> {

//    @EntityGraph(type = EntityGraph.EntityGraphType.FETCH,
//            attributePaths = "roles")
    Optional<UserEnt> findByName(String name);

//    @EntityGraph(type = EntityGraph.EntityGraphType.FETCH,
//            attributePaths = "roles")
    Optional<UserEnt> findByEmail(String email);

//    @EntityGraph(type = EntityGraph.EntityGraphType.FETCH,
//            attributePaths = "roles")
    Optional<UserEnt> findByVerificationCode(String vCode);
}
