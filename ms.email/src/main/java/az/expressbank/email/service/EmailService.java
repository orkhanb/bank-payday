package az.expressbank.email.service;

import az.expressbank.email.model.SendMessageRequestDto;

public interface EmailService {
    void sendVerification(SendMessageRequestDto sendMessageRequestDto);
}
