package az.expressbank.email.service.impl;

import az.expressbank.email.exception.MessagingNotFoundException;
import az.expressbank.email.model.SendMessageRequestDto;
import az.expressbank.email.service.EmailService;
import lombok.RequiredArgsConstructor;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;

@Service
@RequiredArgsConstructor
public class EmailServiceImpl implements EmailService {

    private final JavaMailSender mailSender;

    @Override
    public void sendVerification(SendMessageRequestDto sendMessageRequestDto) {
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);
        try {
            helper.setFrom(sendMessageRequestDto.getFromAddress(), sendMessageRequestDto.getSenderName());
            helper.setTo(sendMessageRequestDto.getToAddress());
            helper.setSubject(sendMessageRequestDto.getSubject());
            helper.setText(sendMessageRequestDto.getContent(), true);
        }catch (MessagingException | UnsupportedEncodingException mEx ){
            throw  new MessagingNotFoundException(mEx.getMessage());
        }
        mailSender.send(message);
    }
}
