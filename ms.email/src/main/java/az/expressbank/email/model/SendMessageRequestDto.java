package az.expressbank.email.model;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SendMessageRequestDto {
    String toAddress;
    String fromAddress;
    String senderName;
    String subject;
    String content;
}
