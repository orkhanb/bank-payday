package az.expressbank.email.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.Serializable;

@Getter
@AllArgsConstructor
public class ErrorResponse implements Serializable {
    private final String message;

    public static ErrorResponse of(Exception e) {
        return new ErrorResponse(e.getMessage());
    }

    public static ErrorResponse of(String errorMessage) {
        return new ErrorResponse(errorMessage);
    }

}
