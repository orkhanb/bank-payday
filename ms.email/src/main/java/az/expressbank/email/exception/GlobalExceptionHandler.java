package az.expressbank.email.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.MailSendException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler(MessagingNotFoundException.class)
    @ResponseStatus(NOT_FOUND)
    public ErrorResponse handle(MessagingNotFoundException exception) {
        log.error(exception.getMessage());
        return ErrorResponse.of(exception);
    }

    @ExceptionHandler(MailSendException.class)
    @ResponseStatus(NOT_FOUND)
    public ErrorResponse handle(MailSendException exception) {
        log.error(exception.getMessage());
        return ErrorResponse.of(exception);
    }

}
