package az.expressbank.email.exception;

public class MessagingNotFoundException extends RuntimeException {
    public MessagingNotFoundException(String str) {
        super(str);
    }
}
