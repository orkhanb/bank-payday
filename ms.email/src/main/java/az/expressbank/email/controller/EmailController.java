package az.expressbank.email.controller;

import az.expressbank.email.annotation.Api;
import az.expressbank.email.model.SendMessageRequestDto;
import az.expressbank.email.service.EmailService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.validation.Valid;

@Api(path = "/ms/email")
@RequiredArgsConstructor
public class EmailController {

    private final EmailService emailService;

    @PostMapping("/verify")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void sendVerification(@RequestBody @Valid SendMessageRequestDto sendMessageRequestDto) {
        emailService.sendVerification(sendMessageRequestDto);
    }
}
